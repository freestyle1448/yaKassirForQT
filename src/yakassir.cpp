#include "yakassir.h"

#include <QDebug>
#include <QUrlQuery>

YaKassir::YaKassir(QObject *parent) : QObject(parent) {

    connect( &m_manager, SIGNAL( finished( QNetworkReply* ) ), SLOT( onFinished( QNetworkReply* ) ) );

}


YaKassir::~YaKassir(){}

    void YaKassir::pay(double amount) {
        QString str = QObject::tr("{\"amount\": {\"value\": \"%1\",\"currency\": \"RUB\"},\"confirmation\": {\"type\": \"redirect\",\"return_url\": \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKp1UzQJ48Fg_EkdpOb562gvV-LAZV_v-qXMyS0Y89uvl-KRL-\"},\"capture\": true,\"description\": \"Заказ №1\"}").arg(amount);
        QByteArray jsonString = str.toLocal8Bit();

        QNetworkRequest request(QUrl("https://payment.yandex.net/api/v3/payments/"));
        request.setHeader(QNetworkRequest::ContentTypeHeader,
            "application/json");

        QString concatenated = "624243:test_ILuU0pMKC_wVQe7uMZsVizXu6B26voHs8Wl0yTGic3g";
        QByteArray data = concatenated.toLocal8Bit().toBase64();
        QString headerData = "Basic " + data;

        request.setRawHeader("Authorization", headerData.toLocal8Bit());
        request.setRawHeader("Idempotence-Key", QString("4799642d-fb54-46f6-a8dd-f40dde289fa8").toLocal8Bit());
        m_manager.post(request, jsonString);
    }

    void YaKassir::onFinished( QNetworkReply* reply ) {
        if( reply->error() == QNetworkReply::NoError ) {
            QString data = QString::fromUtf8( reply->readAll() );
            qInfo() << data ;

            qInfo() << "Ready!" ;
        } else {
            qInfo() << reply->errorString();
        }

        reply->deleteLater();
    }
