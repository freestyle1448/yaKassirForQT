#ifndef YAKASSIR_H
#define YAKASSIR_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class YaKassir : public QObject
{
    Q_OBJECT
private:
    QNetworkAccessManager m_manager;
public:
   YaKassir(QObject *parent = nullptr);
   ~YaKassir();

    Q_INVOKABLE void pay(double amount);
signals:

private slots:
   void onFinished( QNetworkReply* reply );
public slots:
};

#endif // YAKASSIR_H
