<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CoverPage</name>
    <message>
        <source>My Cover</source>
        <translation>Mein Cover</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>UI Template</source>
        <translation>UI-Vorlage</translation>
    </message>
    <message>
        <source>Hello Sailors</source>
        <translation>Hallo Matrosen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>{&quot;amount&quot;: {&quot;value&quot;: &quot;%1&quot;,&quot;currency&quot;: &quot;RUB&quot;},&quot;confirmation&quot;: {&quot;type&quot;: &quot;redirect&quot;,&quot;return_url&quot;: &quot;https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKp1UzQJ48Fg_EkdpOb562gvV-LAZV_v-qXMyS0Y89uvl-KRL-&quot;},&quot;capture&quot;: true,&quot;description&quot;: &quot;Заказ №1&quot;}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SecondPage</name>
    <message>
        <source>Nested Page</source>
        <translation>Unterseite</translation>
    </message>
    <message>
        <source>Item</source>
        <translation>Element</translation>
    </message>
</context>
</TS>
